import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    BASE_URL: 'https://api.themoviedb.org/3',
    API_KEY: '8f70322e2f2306570f0cc2181beeb7da',
    poster_url: 'https://image.tmdb.org/t/p/w780',
    backdrop_url: 'https://image.tmdb.org/t/p/original'
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
