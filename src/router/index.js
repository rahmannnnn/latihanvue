import Vue from 'vue'
import VueRouter from 'vue-router'
import Homepage from '../views/Homepage.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Homepage',
    component: Homepage
  },
  {
    path: '/movie',
    name: 'Movies',
    component: () => import('../views/Movies.vue')
  },
  {
    path: '/tv',
    name: 'Tvshow',
    component: () => import('../views/TVShows.vue')
  },
  {
    path: '/people',
    name: 'people',
    component: () => import('../views/People.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
